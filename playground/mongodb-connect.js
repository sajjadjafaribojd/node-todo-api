/* jshint esversion:6 */

//const MongoClient=require('mongodb').MongoClient;
const {MongoClient,ObjectID}=require('mongodb');

var obj=new ObjectID();
console.log(obj); //create object id with object destructure

//mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
// connect multi server and insert to multi device.

MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,client)=>{
    if(err) {
        return console.log('Unable to connect to MongoDB server.');
    }   
    console.log('Connected to MongoDB server.');
    const db=client.db('TodoApp'); //connect to database.

    // db.collection('Todos').insertOne({ //connect to collection.
    //     text:'Something to do',
    //     completed:false
    // },(err,result)=>{
    //     if(err){
    //         return console.log('Uable to insert todo',err);
    //     }

    //     console.log(JSON.stringify(result.ops,undefined,2));
    // });

    // db.collection('Users').insertOne({
    //     name:'JabJ',
    //     age:30,
    //     location:'iran'
    // },(err,reuslt)=>{
    //     if(err){
    //         return console.log('Unable to insert users',err);
    //     }

    //        console.log(JSON.stringify(reuslt.ops,undefined,2));
    //     //console.log(JSON.stringify(reuslt.ops[0]._id.getTimestamp(),undefined,2)); 
    //     //console.log(JSON.stringify(reuslt.ops[0]._id,undefined,2)); // just show _id in output

    // });

    
    client.close();
});