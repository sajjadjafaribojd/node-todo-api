/* jshint esversion:6 */

const {MongoClient,ObjectId}=require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,client)=>{
    if(err){
        return console.log('Unbale to connect to MongoDB server.',err);
    }
    console.log('Connected to Mongodb server.');
    const db=client.db('TodoApp');

    //deleteOne
    // db.collection('Todos').deleteOne({completed:false}).then((myResult)=>{
    //     //console.log(JSON.stringify(result,undefined,2));
    //     console.log(Object.keys(myResult)); // Object.keys(result) //output: [ 'result', 'connection', 'message', 'deletedCount' ]

        
    //     if(myResult.result.n === 1){
    //         console.log('Delete successfull.');
    //     }else{
    //         console.log('Delete unsuccessfull.');
    //     }
    // },(err)=>{
    //     console.log('Unable to delete.',err);
    // });

    //deleteMany

    // db.collection('Todos').deleteMany({completed:false}).then((myResult)=>{
    //         console.log(Object.keys(myResult));

    //         if(myResult.result.n ===1){
    //             console.log('Delete successfull.');
    //             console.log(`Item delete equal:${myResult.result.n}`);
    //         }
    //       
    // },(err)=>{
    //     console.log('Unable to delete.',err);
    // });


    //findOneAndDelete

    db.collection('Todos').findOneAndDelete({completed:true,text:'jabj'}).then((myResult)=>{
        console.log(Object.keys(myResult)); // [ 'lastErrorObject', 'value', 'ok' ]
        console.log(JSON.stringify(myResult,undefined,2));

        if(myResult.lastErrorObject.n === 1){
            console.log('Delete successfull.');
           
        }else{ 
            console.log('Delete unsuccessfull.');
        }

    },(err)=>{
        console.log('Unable to delete.',err);
    });
});