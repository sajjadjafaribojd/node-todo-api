/* jshint esversion:6 */
const {ObjectID}=require('mongodb');

const {mongoose}=require('./../server/db/mongoose');
const {Todo}=require('./../server/models/todo');
const {User}=require('./../server/models/user');

var id= '5d26471bc1ea7d14896d9e56';
var invalidId='5d26471bc1ea7d14896d9e56aa';

if(!ObjectID.isValid(id)){ // insert invalidId for show message.
    return console.log('ID not valid!');
}

Todo.find({ // if one or more result return all.
    _id:id
}).then((results)=>{
    console.log('Todos - find:',results);
});

Todo.findOne({ //if one or more result return just one.
    _id:id
}).then((result)=>{
    console.log('Todo - find one:',result);
});

Todo.findById(id).then((result)=>{
    console.log('Todo - find by id:',result);
}).catch((e)=>{
    console.log(e);
});



//get user.

var userId='5d2658f8634b582596fbef39';

if(!ObjectID.isValid(userId)){
    return console.log('user ID not valid!');
}

User.findById(userId).then((result)=>{
    console.log(JSON.stringify(result,undefined,2));
}).catch((e)=>{
    console.log(e);
});