/* jshint esversion:6 */

const {MongoClient,ObjectID}=require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,client)=>{
    if(err){
        return console.log('Unable to connect to MongoDB server.');
    }
    console.log('Connect to MongoDB server.');
    const db=client.db('TodoApp'); 

    // db.collection('Todos').findOneAndUpdate(
    //     {_id: new ObjectID('5d1a43aa1c4b95750f8be05b')
    // },{
    //     $set:{ completed:false}
    //     },{
    //         returnNewDocument:true ///????? 
    //     }

    // ).then((myResult)=>{
    //     console.log(Object.keys(myResult));
    //     console.log(myResult);

    // },(err)=>{
    //     console.log('Error:Unbale to update request',err);
    // });
    

    // db.collection('Users').findOneAndUpdate(
    // {
    //     name:'golab'
    // },{
    //     $inc: { // increment - update oprator mongodb.
    //         age: 50
    //     }

    // },{
    //     //returnOriginal:true
    //     //returnNewDocument:false 
    // }
    // ).then((myResult)=>{
    //     console.log(Object.keys(myResult));
    //     console.log(myResult);
    // },(err)=>{
    //     console.log('Error:Unabale to update request'.err);
    // });

    // db.collection('Users').findOneAndUpdate(
    //     {
    //         //Fillter

    //         name:'JabJ1234' // this id does not exist. 
    //     },{
    //         // Update:

    //         $inc: { // increment - update oprator mongodb.
    //             age: 50
    //         },
    //         $set:{
    //             location:'USA',
    //         }
    
    //     },{
    //         //Option:

    //          returnOriginal:true,
    //         //returnNewDocument:false, 
    //          upsert:false 
    //          // because JabJ123 does not exist mongo create new filed. for Avoid doing this should upsert=fasle if upsert=true new filed added.
             
    //     }
    //     ).then((myResult)=>{
    //         console.log(Object.keys(myResult));
    //         console.log(myResult);
    //     },(err)=>{
    //         console.log('Error:Unabale to update request'.err);
    //     });


        db.collection('Users').update(
            {
                name:'JabJ'
            },{
                $set:{
                    location:'fars',
                    age: 111
                }
            },{
                multi:true //
            }
        ).then((myResult)=>{
             console.log(Object.keys(myResult));
             console.log(myResult);
        },(err)=>{
            console.log('Error:Unabale to update request'.err);
        });
});