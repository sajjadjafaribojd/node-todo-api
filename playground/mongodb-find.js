/* jshint esversion:6 */

const {MongoClient,ObjectId}=require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp',(err,client)=>{
    if(err){
        return console.log('Unable to connect to MongoDB server.',err);
    }
    console.log('Connected to MongoDB server.');
    const db=client.db('TodoApp'); // connect to TodoApp database

    db.collection('Users').find({name:'JabJ'}).count().then((count)=>{
        console.log('Users collection:');
        console.log(`JabJ name count ${count}`);
    },(err)=>{
        console.log('Unable to fetch count users.',err);
    });

    db.collection('Users').find({name:'JabJ'}).toArray().then((docs)=>{
        console.log(JSON.stringify(docs,undefined,2));
    },(err)=>{
        console.log('Unable to fetch  users.',err);
    });

    //client.close();
});


