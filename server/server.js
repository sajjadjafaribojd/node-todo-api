/* jshint esversion:6 */
const express=require('express');
const bodyParser=require('body-parser');
const {ObjectID}=require('mongodb');

var {mongoose}=require('./db/mongoose');
var {Todo}=require('./models/todo');
var {User}=require('./models/user');

var app=express();

app.use(bodyParser.json()); //convert string to json.

// todo endpoint
app.post('/todos',(req,res)=>{// app.post(/anyname -this name is for endpoint for user. we can write anything. for eg: jabj or test...)
    var todo=new Todo({
        text:req.body.text
    });

    todo.save().then((myResult)=>{
        //console.log();
        res.send(myResult);
    },(err)=>{
        //console.log(); we should send reslt for user in mobile app ro browser. then we can not user cosole.log for show error ro result.
        res.status(400).send(err);
    });
});

//user endpoint
app.post('/adduser',(req,res)=>{
    var user=new User({
        email:req.body.email,
        username:req.body.username
    });

    user.save().then((doc)=>{
        //console.log();
        res.send(doc);
    },(err)=>{
        //console.log(); we should send reslt for user in mobile app ro browser. then we can not user cosole.log for show error ro result.
        res.status(400).send(err);
    });
});
 
//GET route
app.get('/gettodos',(req,res)=>{
    Todo.find().then((myRes)=>{ // Todo.find() -> mongoose query.
        res.send(myRes);
    },(err)=>{
        res.status(400).send(err); 
    });
});

app.listen(3000,'localhost',()=>{
    console.log('Started on port 3000');
});


//get indivisual information.

app.get('/gettodo/:id',(req,res)=>{
    var id=req.params.id;
    //res.send(req.params); //show output

    if(!ObjectID.isValid(id)){
        return res.status(404).send();
    }

    Todo.findById(id).then((result)=>{
        if(!result){
            return res.status(404).send();
        }

        res.send({result});

    }).catch((e)=>{
        res.status(400).send(err); 
    });
});