/* jshint esversion:6 */

const mongoose=require('mongoose');

var Todo = mongoose.model('Todo',{
    text:{
        type:String,
        required:true, //validator
        minlenght:1, //validator
        trim:true   //validator
        
    },
    completed:{
        type:Boolean,
        default:false
    },
    completedAt:{
        type:Number,
        default:null
    }
});

module.exports={
    //Todo:Todo
    Todo
};    