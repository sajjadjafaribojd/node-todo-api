/* jshint esversion:6 */

const mongoose=require('mongoose');
//import mongoose from 'mongoose';

//mongoose.connect(uri, options, callbackerror)
//https://mongoosejs.com/docs/connections.html#options


/*mongoose.connect('mongodb://localhost:27017/TodoApp').then((myResult)=>{

},(err)=>{
    console.log('Unable to connect MongoDB server.',err);
});*/


//Step 1: connect to mongodb.

mongoose.Promise=global.Promise;
mongoose.connect('mongodb://localhost:27017/TodoApp',{useNewUrlParser: true});

module.exports={
    //mongoose:mongoose  //same below.
      mongoose
};